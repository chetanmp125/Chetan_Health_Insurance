package com.health;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HealthInsurance {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		
		/*THE PROBLEM

		Norman Gomes is looking for a health insurance quote using this application.


		INPUT


		Name: Norman Gomes
		Gender: Male
		Age: 34 years
		Current health:


		Hypertension: No
		Blood pressure: No
		Blood sugar: No
		Overweight: Yes


		Habits:


		Smoking: No
		Alcohol: Yes
		Daily exercise: Yes
		Drugs: No






		OUTPUT


		Health Insurance Premium for Mr. Gomes: Rs. 6,856



		BUSINESS RULES



		Base premium for anyone below the age of 18 years = Rs. 5,000
		% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
		Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
		Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		Habits


		Good habits (Daily exercise) -> Reduce 3% for every good habit
		Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit*/
		
		
		System.out.println("Please tell you details to create your health insurance");
		Scanner scanner = new Scanner(System.in);
		System.out.print("Name:");
		String name = scanner.next();
		
		System.out.print("Gender:");
		String gender = scanner.next();
		
		System.out.print("Age:");
		int age = scanner.nextInt();
		
		
		
		
		
		System.out.println("Current Helath:::::");
		
		
		System.out.print("HyperTension:");
		String hyperTension = scanner.next();
		
		System.out.print("Blood Pressure:");
		String bloodPressure = scanner.next();
		
		System.out.print("Blood Sugar");
		String bloodSugar = scanner.next();
		
		System.out.print("Overweight");
		String overWeight = scanner.next();
		
		
		System.out.println("Habits::::::::::::");
		
		System.out.print("Alcohol:");
		String alcohol = scanner.next();
		
		System.out.print("Smoking:");
		String smoking = scanner.next();
		
		System.out.print("Daily Excercise:");
		String dailyExcercise = scanner.next();
		
		System.out.print("Drugs:");
		String drugs = scanner.next();
		
		double insuranceAmount;
		
		List<String> currHealth = new ArrayList<String>();
		currHealth.add(hyperTension);
		currHealth.add(bloodPressure);
		currHealth.add(bloodSugar);
		currHealth.add(overWeight);
		List<String> habits = new ArrayList<String>();
		habits.add(alcohol);
		habits.add(smoking);
		habits.add(drugs);
		
		
		insuranceAmount =  getPremiumAmt(currHealth,habits,age,gender,dailyExcercise); 
		
		
		System.out.println("Health Insurance Premium for Mr."+name+" : Rs."+insuranceAmount);

	}

	public static  double getPremiumAmt(List<String> currHealth,
			List<String> habits,int age,String gender,String dailyExcercise ) {
		 double amount = 5000.00;
		 if(age<18){
			 return amount;
		 }else if(age >=18){
			 //increase 10%
			 amount += ((amount/100.00) *10);
		 }
		 if(age >=25){
			 amount += ((amount/100.00) *10);
		 }if(age >=30 ){
			 amount += ((amount/100.00) *10);
		 }if(age >=35){
			 //increase 40%
		 }if(age >=40){
			 amount += ((amount/100.00) *10);
		 }
		 System.out.println("age" + amount);
		 
		 
		 
		 if(gender.equalsIgnoreCase("male")){
			 amount += ((amount/100.00)*2.00); 
		 }
		 
		 
		 for(String currHealthCond : currHealth){
			 if(currHealthCond.equalsIgnoreCase("yes")){
				 
				 amount += ((amount/100.00));
				 
			 }
			 
		 }
		 
		 if(dailyExcercise.equalsIgnoreCase("Yes")){
			 amount -= ((amount/100));
			 amount -= ((amount/100));
			 amount -= ((amount/100));
		 }
		 
		 
		 for(String habit : habits){
			 if(habit.equalsIgnoreCase("yes")){
				 
				 amount += ((amount/100.00));
				 amount += ((amount/100.00));
				 amount += ((amount/100.00));
				 
			 }
			 
			 
		 }
		
		
		
				return amount;
	}

}
